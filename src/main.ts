import Vue from 'vue'
import Router from './plugins/router'
import Vuetify from './plugins/vuetify'
import { StateStore } from './core/store/extensions/version/Store'
import './registerServiceWorker'

Vue.config.productionTip = false;

const App = () => import('./App.vue')

new Vue({
  store: StateStore,
  router: Router(StateStore),
  vuetify: Vuetify(StateStore),
  render: (h) => h(App),
}).$mount('#app')
