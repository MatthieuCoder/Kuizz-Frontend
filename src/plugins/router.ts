// @ts-ignore
import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import { AppState } from '../core/store/extensions/version/Store'
import { Store } from 'vuex/types';
import { InAppNotification } from '@/core/store/extensions/version/Notifications';

Vue.use(VueRouter);

type KuizzRoute = RouteConfig & {
  authRequired?: boolean,
}

const routes: KuizzRoute[] = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/common/Home.vue'),
  },
  {
    path: '/leaderboard',
    name: 'Leaderboard',
    component: () => import('@/views/Leaderboard.vue'),
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import('@/views/account/Settings.vue'),
  },
  {
    path: '/tos',
    name: 'Terms of Service',
    component: () => import('@/views/pages/TOS.vue'),
  },
  {
    path: '/privacy-policy',
    name: 'Privacy policy',
    component: () => import('@/views/pages/PrivacyPolicy.vue'),
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('@/views/pages/About.vue'),
  },
  {
    path: '/thanks',
    name: 'Thanks',
    component: () => import('@/views/pages/Dedicaces.vue'),
    authRequired: true,
  },
  {
    path: '/login-callback',
    name: 'Callback',
    component: () => import('@/views/account/LoginCallback.vue'),
  },
  {
    path: '/users/:id',
    name: 'User',
    component: () => import('@/views/account/User.vue'),
  },
  { path: '*', redirect: '/' },
]

export default (store: Store<AppState>) => {
  const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
  })

  router.beforeEach((to, from, next) => {
    const needLogin = routes.some((x) => x.authRequired && x.path === to.path)
    if (!store.state.Session.loggedIn && needLogin) {
      store.dispatch('Notifications/push', {
        timeout: 5000,
        message: 'You aren\'t allowed to view this page! 😥',
        color: 'red',
      } as InAppNotification)
      return next('/')
    }
    next()
  })

  return router
}
