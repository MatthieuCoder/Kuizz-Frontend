import jwtDecode from 'jwt-decode'

import { API_ENDPOINT } from '@/core/api/index'

export function openLoginPopup(provider: string, linkAccount: string | null = null): Promise<{ token: string }> {
    const h = 700,
        w = 500,
        left = (screen.width / 2) - (w / 2),
        top = (screen.height / 2) - (h / 2),
        url = encodeURI(`${API_ENDPOINT}/oauth/${provider}?redirect=${window.location.origin}/login-callback${ linkAccount ? `&linkAccount=${linkAccount}` : '' }`)

    return new Promise((resolve, reject) => {
        const win = window.open(url, 'Kuizz - Login', 'modal=yes, dialog=yes,toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        if (!win) {
            return reject('Invalid cannot open the window.')
        }
        let done = false
        window.addEventListener('message', (event) => {
            if (event.isTrusted) {
                try {
                    const token = event.data
                    const user = jwtDecode(token) as any
                    done = true
                    win.close()
                    user.token = token
                    resolve(user)
                } catch (e) {/* Error? */}
            }
        }, false)
        win.addEventListener('close', () => {
            if (!done) {
                reject('Used declined.')
            }
        })
    })
}
