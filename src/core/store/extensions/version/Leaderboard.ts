import User from '@/core/api/typings/User'
import { Module, ActionContext } from 'vuex'
import { AppState } from './Store'
import { InAppNotification } from './Notifications'
import { getLeaderboard } from '@/core/api/Leaderboard'

export class LeaderboardState {
    public error?: any | null
    public leaderboard?: User[] | null
    public loaded: boolean = false
}

export default class LeaderboardStore implements Module<LeaderboardState, AppState> {
    public namespaced = true

    public mutations = {
        preLoad(state: LeaderboardState) {
            state.error = null
            state.leaderboard = null
            state.loaded = false
        },
        loaded(state: LeaderboardState, leaderboard: User[]) {
            state.error = null
            state.leaderboard = leaderboard
            state.loaded = true
        },
        failed(state: LeaderboardState, error: any) {
            state.error = error
            state.leaderboard = null
            state.loaded = false
        },
    }

    public actions = {
        async load({ commit, dispatch }: ActionContext<LeaderboardState, AppState>) {
            commit('preLoad')
            getLeaderboard()
                .then((leaderboard) => {
                    commit('loaded', leaderboard)
                })
                .catch((error) => {
                    commit('failed', error)
                    dispatch('Notifications/push', {
                        timeout: 5000,
                        message: `Failed to load the leaderboard : ${error}`,
                        color: 'red',
                    } as InAppNotification, { root: true })
                })
        },
    }
    public state = () => new LeaderboardState()
}
