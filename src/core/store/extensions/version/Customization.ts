import { Module, ActionContext } from 'vuex'
import { AppState } from './Store'

export class CustomizationState {
    public darkTheme: boolean = true
    public navBarOpenned: boolean = false
}

export default class CustomizationStore implements Module<CustomizationState, AppState> {
    public namespaced = true
    public mutations = {
        setDarktheme(state: CustomizationState, dark: boolean) {
            state.darkTheme = !state.darkTheme
        },
    }
    public actions = {
        setDarkTheme({ commit }: ActionContext<CustomizationState, AppState>, dark: boolean) {
            commit('setDarktheme', dark)
        },
    }
    public state = () => new CustomizationState()
}
