import Vuex, { Store } from 'vuex'
import SecureLS from 'secure-ls'
import Vue from 'vue'
import createPersistedState from 'vuex-persistedstate'
import Session, { SessionState } from './Session'
import Customization, { CustomizationState } from './Customization'
import Axios from 'axios'
import Notifications from './Notifications'


const secureLocalStorage = new SecureLS({ isCompression: false })

export class AppState {
    public Customization: CustomizationState = new CustomizationState()
    public Session: SessionState = new SessionState()
}

Vue.use(Vuex)

export const StateStore = new Store<AppState>({
    state: () => new AppState(),
    modules: {
        Session: new Session(),
        Customization: new Customization(),
        Notifications: new Notifications(),
    },
    plugins: [
        createPersistedState({
            storage: {
                getItem: (key) => secureLocalStorage.get(key),
                setItem: (key, value) => secureLocalStorage.set(key, value),
                removeItem: (key) => secureLocalStorage.remove(key),
            },
        }),
    ],
})

export default StateStore

Axios.interceptors.request.use(async (request) => {
    request.headers['authorization'] = `Bearer ${StateStore.state.Session.token}`
    return request
})

Axios.interceptors.response.use(async (response) => {
    if (response.status === 401) {
        StateStore.dispatch('Session/logout')
    }
    return response
})
