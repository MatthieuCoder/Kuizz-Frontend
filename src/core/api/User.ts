import { API_ENDPOINT } from '.'
import User from './typings/User'
import Axios from 'axios'

export function getProfile(user = '@me'): Promise<User> {
    return Axios(`${API_ENDPOINT}/api/users/${user}`)
        .then((x) => x.data)
}

export function deleteProfile(user = '@me'): Promise<User> {
    return Axios(`${API_ENDPOINT}/api/users/${user}`, { method: 'DELETE' })
        .then((x) => x.data)
}
